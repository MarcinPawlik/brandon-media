<?php

//input data
$input = [
    'chleb' => ['auchan' => 2.3, 'biedronka' => 2.6, 'lidl' => 2.4],
    'maslo' => ['auchan' => 2.5, 'biedronka' => 3, 'lidl' => 2.4, 'zabka' => 3.5],
    'jogurt' => ['auchan' => 1.5, 'lidl' => 1.4, 'zabka' => 2],
    'ser' => ['auchan' => 1.3, 'lidl' => 1.9, 'zabka' => 1.5, 'biedronka' => 1.1]
];
$cart = ['chleb', 'maslo'];

//output code

//map data
foreach ($input as &$products) {
  asort($products); //sort by price
}

//calc data
$best = [];

foreach ($cart as $product) {
  if(sizeof($input[$product]) == 0){ //when any shop dont have a product
    continue;
  }
  $bestShop = array_keys($input[$product])[0];
  $bestPrice = array_values($input[$product])[0];

  if(!array_key_exists($bestShop, $best)){
    $best[$bestShop] = [];
  }

  $best[$bestShop][$product] = $bestPrice;
}

//print result
foreach ($best as $shop => $products) {
  echo $shop.":\n";
  foreach ($products as $product => $price) {
    echo "* $product $price \n";
  }
  echo "\n";
}
