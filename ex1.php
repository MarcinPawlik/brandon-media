<?php

//input data
$input = [
    'chleb' => ['auchan' => 2.3, 'biedronka' => 2.6, 'lidl' => 2.4],
    'maslo' => ['auchan' => 2.5, 'biedronka' => 3, 'lidl' => 2.4, 'zabka' => 3.5],
    'jogurt' => ['auchan' => 1.5, 'lidl' => 1.4, 'zabka' => 2],
    'ser' => ['auchan' => 1.3, 'lidl' => 1.9, 'zabka' => 1.5, 'biedronka' => 1.1]
];
$cart = ['chleb', 'maslo'];

//output code
$shops = [];

//map data
foreach ($cart as $product) {
  foreach ($input[$product] as $shop => $price) {
      $shops[$shop][] = ['product' => $product, 'price' => $price];
  }
}

//calc sum
$sumArray = [];
foreach ($shops as $shop => &$products) {
  if(sizeof($products) != sizeof($cart)){ //remove whan dont have a product
    unset($shops[$shop]);
    continue;
  }
  $sum = 0;
  foreach ($products as $product) {
    $sum += $product['price'];
  }
  $sumArray[$shop] = $sum;
}

asort($sumArray); //sort by sum

//print result
foreach ($sumArray as $shop => $sum) {
  echo $shop.":\n";
  foreach ($shops[$shop] as $product) {
    echo "* ".$product['product']." ".$product['price']."\n";
  }
  echo "* suma ".$sum."\n\n";
}
