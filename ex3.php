<?php

//input data
$input = [
    'chleb' => ['auchan' => 2.3, 'biedronka' => 2.6, 'lidl' => 2.4],
    'maslo' => ['auchan' => 2.5, 'biedronka' => 3, 'lidl' => 2.4, 'zabka' => 3.5],
    'jogurt' => ['auchan' => 1.5, 'lidl' => 1.4, 'zabka' => 2],
    'ser' => ['auchan' => 1.3, 'lidl' => 1.9, 'zabka' => 1.5, 'biedronka' => 1.1]
];
$cart = ['chleb', 'maslo'];

//output code

class shop{

  private $input;
  private $cart;
  private $lowPrice = [];
  private $allShop = [];
  private static $MAX_PRICE = 999999; //spme max value

  function __construct($input, $cart){
    $this->input = $input;
    $this->cart = $cart;

    $this->getShops();
    $this->map();
  }

  public function calc(){
    $best = [];
    foreach ($this->allShop as $s1) {
      foreach ($this->allShop as $s2) {
        $best[] = $this->bestPrice($s1, $s2);
      }
    }
    $result = $this->choiseBestPrice($best);
    $this->printResult($result);
  }

  private function bestPrice($shop1, $shop2){
    $bestPrice = [];
    foreach ($this->lowPrice[$shop1] as $product1 => $price1) {
      foreach ($this->lowPrice[$shop2] as $product2 => $price2) {
        if($product1 == $product2){
          if($price1 < $price2){
            $bestPrice[$shop1][$product1] = $price1;
          }else{
            $bestPrice[$shop2][$product2] = $price2;
          }
        }
      }
    }
    return $bestPrice;
  }

  private function choiseBestPrice($bestPrice){
    $gSum = self::$MAX_PRICE;
    $best = [];

    foreach ($bestPrice as $bs) {
      $sum = 0;
      foreach ($bs as $shop => $products) {
        foreach ($products as $product => $price) {
          $sum += $price;
        }
      }
      if($gSum > $sum){
        $gSum = $sum;
        $best = $bs;
      }
    }

    return $best;
  }

  private function getShops(){
    $shops = [];
    foreach ($this->input as $array) {
      foreach ($array as $shop => $price) {
        if(!in_array($shop, $shops)){
          $shops[] = $shop;
        }
      }
    }
    $this->allShop = $shops;
  }

  private function printResult($result){
    foreach ($result as $shop => $products) {
      echo $shop.":\n";
      foreach ($products as $product => $price) {
        echo "* $product $price \n";
      }
      echo "\n";
    }
  }

  private function map(){
    foreach ($this->cart as $product) {
      foreach ($this->allShop as $shop) {
        if(array_key_exists($shop, $this->input[$product])){
          $productPrice = $this->input[$product][$shop];
        }else{
          $productPrice = self::$MAX_PRICE; //some big
        }

        $this->lowPrice[$shop][$product] = $productPrice;
      }
      // foreach ($this->input[$product] as $shop => $productPrice) {
      //   // echo "$shop => $productPrice \n";
      //   $this->lowPrice[$shop][$product] = $productPrice;
      //
      //   if(!in_array($shop, $this->allShop)){
      //     $this->allShop[] = $shop;
      //   }
      // }
      // asort($this->lowPrice[$shop]);
    }
  }

}



$s = new shop($input, $cart);
$s->calc();
